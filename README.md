# HCE Gitlab Integration Demo Block

## Objective

- Illustrate how users can use Harness Chaos Engineering to run chaos experiments in a Gitlab CI/CD pipeline & how to rollback deployments based on chaos testing outcomes. 

## Things You Need

- A Gitlab account with CI/CD features enabled (this demo steps were performed during the unlimited trial period of the [free plan](https://about.gitlab.com/pricing/))

- A Kubernetes cluster on which (an instrumented version of) the sample microservices application [online-boutique](https://github.com/GoogleCloudPlatform/microservices-demo) is deployed. Also, **kubectl** in your workspace. 

  - The pipeline will deploy, chaos test & conditionally rollback one of the microservices, named "cartservice" in this cluster. 

- [Supported version of Helm](https://helm.sh/docs/topics/version_skew/) (in order to setup the gitlab agent on the cluster) 

## Configuration 

### Step-1: Gitlab

- Fork this repository into your own Gitlab Org/account

- Setup the Gitlab Agent in your Kubernetes cluster with the name `gitlab-agent`. 

  - To do this, refer to the instructions here: https://docs.gitlab.com/ee/user/clusters/agent/install/

  - If you are in a hurry, perform these steps: 

    - Click on the **Add Kubernetes Cluster** button available in the repo header

    - Follow along to **connect a cluster**, access the dropdown and type the name `gitlab-agent` in the **enter a name to create new** field, select **create agent** option & finally click **Register**

    - Copy the command in the **Install using Helm (recommended)** 

### Step-2: Kubernetes 

- Execute the helm install command to setup the Gitlab agent in the appropriate Kube-context. Note that this command has your Agent Access Token embedded within it. 

- Follow instructions in the [Creating a demo application and observability infrastructure](https://developer.harness.io/tutorials/run-chaos-experiments/first-chaos-engineering#creating-a-demo-application-and-observability-infrastructure) to setup the Online-Boutique demo application


### Step-3: Harness 

- Signup to the [Harness platform](https://app.harness.io) via email 

- Click on the verification email received. 

- Choose the Chaos Engineering Module. This will enable a 14-day enterprise trial license.

- You will see a modal asking to to "Enable Chaos Infrastructure To Run Your First Chaos Experiment". Proceed with setting up of the chaos infrastructure on the "default project". You can create a dedicated/new project if you wish. To setup the chaos infra, follow the steps outlined in [Connect Chaos Infrastructures](https://developer.harness.io/docs/chaos-engineering/user-guides/connect-chaos-infrastructures). 

  **Note**: Use the same Kubernetes cluster in which the Gitlab agent has been setup in Step-1/2. 

- Add a new chaoshub by following the steps outlined in [Add Chaos Hub](https://developer.harness.io/docs/chaos-engineering/user-guides/add-chaos-hub) by using the GitHub repo URL https://github.com/ksatchit/boutique-chaos-demo

- Execute the chaos experiment **boutique-carts-cpu-starvation** following steps provided in [Launch Experiment From ChaosHub](https://developer.harness.io/docs/chaos-engineering/user-guides/construct-and-run-custom-chaos-experiments#launch-an-experiment-from-chaos-hub)

#### What Happens In This Chaos Experiment

- We hog the cpu resources in the pod belonging to the carts microservice, simulating a high-traffic situation in which the service is deprived of cpu cycles, leading to slower responses. The intent is to evaluate whether the slowness is handled within the system OR is propagated upwards to cause degraded user experience on the website's transactions.

- During this experiment, we validate the following hypotheses/constraints using "Resilience Probes":

  - Healthy Kubernetes resource status prior to and after fault injection
  - Continuous availability of the microservice under test
  - Expected levels of latency on the website upon user actions (simulated via loadgenerator)

****IMPORTANT: Note The Following Details From Your Harness Account****

1. **Account Scope Details** 

- The Account ID 
- The Project ID 
- The Chaos Experiment ID

  - These can be obtained from your session URL. For example, in this URL:
  
    `https://app.harness.io/ng/#/account/**JxE3EzyXSmWugTiJV48n6K**/chaos/orgs/default/projects/**default_project**/experiments/**d7c9d243-0219-4f7c-84g2-3004e59e4605**` 
   
    The strings marked in asterisk are the account, project & chaos experiment IDs respectively.  

2. **Account API-Key**

- From your account profile page, generate an API-Key & note the Token value. 

### Step-4: Gitlab Pipeline Settings 

- Add the information gathered from the Harness platform in the previous step (ACCOUNT_ID, PROJECT_ID, API_KEY) as [pipeline variables](https://docs.gitlab.com/ee/ci/variables/?_gl=1*ysqeh0*_ga*MTc2NzQ4NTYwLjE2NjQ4MDQ0NjI.*_ga_ENFH3X7M5Y*MTY4MDE0MTE5NC42LjEuMTY4MDE0NDgxNS4wLjAuMA..#for-a-project) ast project scope.

- Replace the `WORKFLOW_ID` value in the [.gitlab.ci](https://gitlab.com/ksatchit/hce-gitlab-integration-demo/-/blob/main/.gitlab-ci.yml#L43) yaml with the value of the Chaos Experiment ID. 

## Pipeline Execution With Chaos Steps 

- Make a commit on the [cartservice deployment spec](https://gitlab.com/ksatchit/hce-gitlab-integration-demo/-/blob/main/k8s/cartservice.yaml) to change the image to `karthiksatchitanand/cartservice:cra-0.1.0`

- Observe the triggered pipeline deploying the application changes & executing the chaos experiment. Navigate to the Harness platform to view the chaos experiment execution in progress

- Observe the Grafana dashboard to see the impact of chaos 

- Verify execution of the deployment rollback step on account of a less-than-expected [Resilience Score](https://developer.harness.io/docs/chaos-engineering/user-guides/manage-chaos-experiment-execution/analyze-chaos-experiment). 


### How It Works 

- The chaos job in the pipeline invokes bash scripts which in turn leverages a CLI tool `hce-saas-api` to make API calls to launch, monitor and derive the resilience score for the chaos experiment. 

- This CLI tool takes the account details, API key (which we have stored as pipeline variables) and the chaos experiment ID (job-env var) as arguments.

<img width="729" alt="Screenshot 2023-03-30 at 7 51 28 PM" src="https://user-images.githubusercontent.com/21166217/228867464-1c1a066f-d943-4d6a-a54d-8c13a622c1e6.png">

